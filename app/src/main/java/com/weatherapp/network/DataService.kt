package com.weatherapp.network

import com.weatherapp.model.WeatherInfo
import retrofit2.Call
import retrofit2.http.POST
import retrofit2.http.Query

interface DataService {
    @POST("onecall")
    fun GetWeaherData(
        @Query("lat") lat: String,
        @Query("lon") lon: String,
        @Query("appid") apiKey: String,
        @Query("exclude") exclude: String
    ): Call<WeatherInfo>
}