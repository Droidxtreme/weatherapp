package com.weatherapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.weatherapp.R
import com.weatherapp.dataStore.LocationData
import com.weatherapp.ui.uiListner.ClickListner
import kotlinx.android.synthetic.main.item_bookmark.view.*

class BookMarkAdapter(
    private var list: List<LocationData>,
    private var mContext: Context,
    private var listner: ClickListner
) :
    RecyclerView.Adapter<BookMarkAdapter.BookMarkHolder>() {

    class BookMarkHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun binData(data: LocationData, listner: ClickListner) {
            itemView.tvBookMarkName.text = data.name
            itemView.tvBookMarkName.setOnClickListener { listner.onclick(data) }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BookMarkAdapter.BookMarkHolder {
        val view: View =
            LayoutInflater.from(parent.context).inflate(R.layout.item_bookmark, parent, false)
        return BookMarkHolder(view)
    }

    override fun onBindViewHolder(holder: BookMarkAdapter.BookMarkHolder, position: Int) {
        holder.binData(list[position], listner = listner)
    }

    override fun getItemCount(): Int {
        return list.size
    }
}