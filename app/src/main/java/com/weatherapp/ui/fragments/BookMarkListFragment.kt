package com.weatherapp.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.weatherapp.R
import com.weatherapp.adapter.BookMarkAdapter
import com.weatherapp.dataStore.AppDatabase
import com.weatherapp.dataStore.LocationData
import com.weatherapp.global.executeAsyncTask
import com.weatherapp.global.toast
import com.weatherapp.ui.listner.SwipeToDeleteCallback
import com.weatherapp.ui.uiListner.ClickListner
import kotlinx.android.synthetic.main.fragment_book_mark_list.*

class BookMarkListFragment : Fragment(), ClickListner {
    private val listLocation: MutableList<LocationData> = mutableListOf()
    private lateinit var bookmarkAdapter: BookMarkAdapter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_book_mark_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        retriveDataAndDisPlay()
    }

    private fun init() {
        bookmarkAdapter = activity?.let { BookMarkAdapter(listLocation, it, this) }!!
        rvBookMartList.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = bookmarkAdapter
        }
        button.setOnClickListener {
            val action =
                BookMarkListFragmentDirections.actionBookMarkListFragmentToLocationDetailsFragment()
            view?.findNavController()?.navigate(action)
        }
        enableSwipeToDelete()
    }

    private fun retriveDataAndDisPlay() {
        lifecycleScope.executeAsyncTask(
            onPreExecute = {},
            onPostExecute = {
                bookmarkAdapter.notifyDataSetChanged()
                if (listLocation.size == 0) {
                    activity?.toast(getString(R.string.alert_no_bookmark))
                }
            },
            doInBackground = {
                listLocation.clear()
                activity?.let { AppDatabase.getInstance(it).locationDao().getAll() }?.let {
                    listLocation.addAll(
                        it
                    )
                }
            })
    }

    private fun enableSwipeToDelete() {
        val swipeToDeleteCallback: SwipeToDeleteCallback =
            object : SwipeToDeleteCallback(activity) {
                override fun onSwiped(viewHolder: RecyclerView.ViewHolder, i: Int) {
                    removeData(viewHolder.adapterPosition)

                }
            }
        val itemTouchhelper = ItemTouchHelper(swipeToDeleteCallback)
        itemTouchhelper.attachToRecyclerView(rvBookMartList)
    }

    private fun removeData(pos: Int) {
        lifecycleScope.executeAsyncTask(
            onPreExecute = {},
            onPostExecute = {
                activity?.toast(getString(R.string.alet_delete))
                listLocation.removeAt(pos)
                bookmarkAdapter.notifyItemRemoved(pos)
            },
            doInBackground = {
                activity?.let {
                    AppDatabase.getInstance(it).locationDao()
                        .delete(listLocation[pos])
                }
            })
    }

    override fun onclick(locationInfo: LocationData) {
        val action =
            BookMarkListFragmentDirections.actionBookMarkListFragmentToWeatherFragment(locationInfo)
        view?.findNavController()?.navigate(action)
    }
}