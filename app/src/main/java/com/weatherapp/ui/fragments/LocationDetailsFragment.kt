package com.weatherapp.ui.fragments

import android.app.AlertDialog
import android.content.DialogInterface
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnMapLoadedCallback
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.weatherapp.R
import com.weatherapp.dataStore.AppDatabase
import com.weatherapp.dataStore.LocationData
import com.weatherapp.global.appName
import com.weatherapp.global.executeAsyncTask
import com.weatherapp.global.hasInternet
import com.weatherapp.global.toast
import java.util.*


class LocationDetailsFragment : Fragment(), OnMapReadyCallback {
    private val TAG = javaClass.simpleName
    private val listOfLocation: MutableList<LocationData> = mutableListOf()
    private lateinit var gMap: GoogleMap

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_location_details, container, false)
    }


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.hasInternet()
        val mapFragment =
            childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)

    }

    override fun onMapReady(googleMap: GoogleMap?) {
        if (googleMap != null) {
            this.gMap = googleMap
        }
//        plotPins(googleMap)
        gMap.setOnMapClickListener {
            Log.e(TAG.toString(), "onMapReady: $it")
            alertToAddAsBookMark(googleMap, it)
        }
        gMap.setOnMarkerClickListener(object : GoogleMap.OnMarkerClickListener {
            override fun onMarkerClick(p0: Marker?): Boolean {
                val locationInfo: LocationData = p0?.tag as LocationData
                val action =
                    LocationDetailsFragmentDirections.actionLocationDetailsFragmentToWeatherFragment(
                        locationInfo
                    )
                view?.findNavController()?.navigate(action)
                return false
            }
        })

        plotSaveLocationWithMarker()
    }

    private fun placeMarker(latLng: LatLng, locationData: LocationData) {
        gMap.apply {
            addMarker(
                MarkerOptions()
                    .position(latLng)
            ).tag = locationData
            moveCamera(CameraUpdateFactory.newLatLng(latLng))
        }
    }

    private fun plotSaveLocationWithMarker() {
        lifecycleScope.executeAsyncTask(
            onPreExecute = {},
            onPostExecute = {
                if (listOfLocation.size > 0) {
                    val builder = LatLngBounds.Builder()
                    for (location in listOfLocation) {
                        placeMarker(LatLng(location.lat, location.longitude), location)
                        builder.include(LatLng(location.lat, location.longitude))
                    }
                    val bounds = builder.build()
                    val padding = 100
                    gMap.setOnMapLoadedCallback(OnMapLoadedCallback {
                        gMap.animateCamera(
                            CameraUpdateFactory.newLatLngBounds(
                                bounds,
                                padding
                            )
                        )
                    })
                }
            },
            doInBackground = {
                activity?.let { AppDatabase.getInstance(it).locationDao().getAll() }?.let {
                    listOfLocation.addAll(
                        it
                    )
                }
            })


    }

    private fun alertToAddAsBookMark(googleMap: GoogleMap?, latLng: LatLng) {
        val dialogBuilder = AlertDialog.Builder(activity)
        dialogBuilder.apply {
            setMessage("Are you sure you want to add this location to bookmark?")
            setCancelable(false)
            setPositiveButton("Yes", DialogInterface.OnClickListener { dialog, which ->
                getLocationInfo(latLng)
            })
            setNegativeButton("No", DialogInterface.OnClickListener { dialog, which ->
                dialog.cancel()
            })
        }
        val alert = dialogBuilder.create()
        alert.setTitle(activity?.appName())
        alert.show()
    }

    fun getLocationInfo(locCorndinate: LatLng) {
        val geocoder: Geocoder
        val addresses: List<Address>
        geocoder = Geocoder(activity, Locale.getDefault())

        addresses = geocoder.getFromLocation(
            locCorndinate.latitude,
            locCorndinate.longitude,
            1
        ) // Here 1 represent max location result to returned, by documents it recommended 1 to 5
        val address =
            addresses[0].getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
        var locationName: String = ""
        when {
            addresses[0].locality != null -> {
                locationName = addresses[0].locality
            }
            addresses[0].adminArea != null -> {
                locationName = addresses[0].adminArea

            }
            addresses[0].countryName != null -> {
                locationName = addresses[0].countryName

            }
            addresses[0].adminArea != null -> {
                locationName = addresses[0].adminArea

            }
            addresses[0].featureName != null -> {
                locationName = addresses[0].featureName

            }
        }
        placeMarker(
            locCorndinate, LocationData(
                name = locationName,
                lat = locCorndinate.latitude,
                longitude = locCorndinate.longitude
            )
        )
        activity?.toast("$locationName")
        lifecycleScope.executeAsyncTask(
            onPreExecute = {},
            onPostExecute = {},
            doInBackground = {
                activity?.let {
                    AppDatabase.getInstance(it).locationDao()
                        .insert(
                            LocationData(
                                name = locationName,
                                lat = locCorndinate.latitude,
                                longitude = locCorndinate.longitude
                            )
                        )
                }
            })
    }

}