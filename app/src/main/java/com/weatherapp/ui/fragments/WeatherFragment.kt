package com.weatherapp.ui.fragments

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.weatherapp.R
import com.weatherapp.dataStore.LocationData
import com.weatherapp.global.gone
import com.weatherapp.global.hasInternet
import com.weatherapp.model.Current
import com.weatherapp.model.WeatherInfo
import com.weatherapp.network.DataService
import com.weatherapp.network.RetrofitClientInstance
import kotlinx.android.synthetic.main.fragment_weather.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class WeatherFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_weather, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (activity?.hasInternet()!!) {
            arguments?.let { getData(WeatherFragmentArgs.fromBundle(it).locationData) }
        }
    }

    private fun getData(location: LocationData) {
        tvCity.text = location.name
        val dataService: DataService = RetrofitClientInstance.getRetrofitInstance()
            .create(DataService::class.java)
        val responseCall: Call<WeatherInfo> =
            dataService.GetWeaherData(
                lat = "${location.lat}",
                lon = "${location.longitude}",
                apiKey = "c5893b3b6561e5c82677f8a6ac2d7da5",
                exclude = "minutely,hourly,alerts"
            )
        responseCall.enqueue(object : Callback<WeatherInfo> {
            override fun onResponse(call: Call<WeatherInfo>, response: Response<WeatherInfo>) {
                progressBar.gone()
                populateData(response.body()?.current)
            }

            override fun onFailure(call: Call<WeatherInfo>, t: Throwable) {
                progressBar.gone()
            }
        })
    }

    private fun populateData(current: Current?) {
        tvValTemp.text = "${current?.temp}"
        tvValHumidity.text = "${current?.humidity}"
        tvValRain.text = "${current?.weather?.get(0)?.description}"
        tvValWind.text = "${current?.windSpeed}"
    }
}
