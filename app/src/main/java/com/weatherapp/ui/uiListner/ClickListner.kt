package com.weatherapp.ui.uiListner

import com.weatherapp.dataStore.LocationData

interface ClickListner {
    fun onclick(locationInfo: LocationData)
}