package com.weatherapp.dataStore

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class LocationData(
    @PrimaryKey
    val locationId: Long = System.currentTimeMillis(),
    @ColumnInfo(name = "location_name") val name: String,
    @ColumnInfo(name = "latitude") val lat: Double,
    @ColumnInfo(name = "longitude") val longitude: Double
) : Parcelable {
}