package com.weatherapp.dataStore

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface LocationDao {
    @Query("SELECT * FROM LocationData")
    fun getAll(): MutableList<LocationData>

    @Insert
    fun insert(vararg location: LocationData)

    @Delete
    fun delete(location: LocationData)
}